=======
History
=======

0.1.0
------------------

* First release on PyPI.

1.0.0
------------------
Fixed an issue with the parser not handling the baseExtension
element contents correctly (eventDeclarations and eventID).
